export class Stock {
  favorite: boolean = false;

  /*
   * Use TypeScript’s syntactic sugar to automatically
   * create the corresponding properties based on the constructor arguments
   * by using the public keyword.
   *
   * */
  constructor(
    public name: string,
    public code: string,
    public price: number,
    public previousPrice: number)
  {}

  isPositiveChange() : boolean {
    return this.price >= this.previousPrice;
  }
}
